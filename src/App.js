import { Component } from 'react';
import Modal from './components/Modal';
import Button from './components/Button';
import ProductList from './components/products/products';
import Header from './components/Header';

class App extends Component {
  constructor() {
    super()
    this.state = {
      products: [],
      cart: JSON.parse(localStorage.getItem("cart")) || [],
      fav: JSON.parse(localStorage.getItem("fav")) || [],
      currentProductId: null,
      modal: {
        id: 0,
        title: 'Do you want to add to cart',
        text: 'you are going to add this to cart',
        isActive: false,
        hasCloseButton: true,
        hideFn: () => this.hideModal(),
      }
    }
  }
  componentDidMount() {
    const getData = async () => {
      const res = await fetch("./data-base.json")
      const data = await res.json()
      const updatedData = this.updateData(data.productList)
      this.setState((prev) => {

        const newState = { ...prev }
        newState.products = updatedData
        return newState
      })
    }
    getData()
  }
  hideModal() {
    this.setState(current => {
      const newState = { ...current };
      newState.modal.isActive = false;
      newState.currentProductId = null
      return newState;
    });
  }
  showModal(id) {

    this.setState(current => {
      const newState = { ...current };
      newState.modal.isActive = true;
      newState.currentProductId = id
      return newState;
    });
  }
  updateData(products) {

    const cart = products.map((item) => {
      if (this.state.cart.includes(item.id)) {
        item.isInCart = true
        return item
      }

      return item
    })

    const fav = cart.map((item) => {
      if (this.state.fav.includes(item.id)) {
        item.isInFav = true
        return item
      }
      return item
    })
    return fav
  }
  toggleInFavorite(id) {
    this.setState((prev) => {
      const newState = { ...prev }
      const findedIdinFav = newState.fav.find((item) => item === id)
      if (!findedIdinFav) {
        newState.fav = [...newState.fav, id]
        newState.products = newState.products.map((item) => {
          if (item.id === id) {
            item.isInFav = true
            return item
          }
          return item
        })
      }
      if (findedIdinFav) {

        newState.fav = newState.fav.filter((item) => item !== id)
        newState.products = newState.products.map((item) => {
          if (item.id === id) {
            item.isInFav = false

            return item
          }
          return item
        })
      }
      localStorage.setItem("fav", JSON.stringify(newState.fav))
      return newState
    })
  }


  addToCart(id) {
    const findedId = this.state.cart.find((item) => item.id === id)
    this.setState((prev) => {
      const newState = { ...prev }
       
      if (!findedId) {
       
        newState.cart = [...newState.cart, { id, counter: 1 }]
      }
      if (findedId) {
        let counter = findedId.counter + 1
        const findedIndex = newState.cart.findIndex((item) => item.id === id)

        newState.cart = newState.cart.toSpliced(findedIndex, 1, { id, counter })
        
      }
      newState.modal.isActive = false
      newState.currentProductId = null
      localStorage.setItem("cart", JSON.stringify(newState.cart))
      return newState
    })

  }

  render() {
    const { products, modal, currentProductId,cart,fav } = this.state;

    return (
      <div className="App">
        <Header favCount ={fav.length} cartCount={cart}/>
        <ProductList toggleInFavorite={this.toggleInFavorite.bind(this)} showModal={this.showModal.bind(this)} products={products} />
        {modal.isActive ? <Modal hideModal={this.hideModal.bind(this)} addToCart={this.addToCart.bind(this)} current={currentProductId} {...modal} /> : null}
      </div>)
  }
}

export default App;

