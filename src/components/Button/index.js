import { PureComponent } from "react";
import { StyledButton } from "./styled";
import PropTypes from "prop-types";

class Button extends PureComponent {
  render() {
    const { onClick, text, backgroundColor} = this.props
    return <StyledButton onClick={onClick} $bgColor={backgroundColor} >{text}</StyledButton>
  } 
}
Button.propTypes = {
  onClick:PropTypes.func,
  text:PropTypes.string,
  backgroundColor:PropTypes.string
}
export default Button;