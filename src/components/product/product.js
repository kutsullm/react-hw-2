import { PureComponent } from "react"
import Button from "../Button"
import { AiOutlineHeart,AiFillHeart } from 'react-icons/ai';
import { StyledImg } from "./styled";
import PropTypes from "prop-types";

class Product extends PureComponent {
    render() {
        const {title,url,article,price,description,onClick,id,toggleInFavorite,isInFav} = this.props
        return(
          <li>
            <p>{isInFav? <AiFillHeart onClick={()=>{toggleInFavorite(id)}}/>:<AiOutlineHeart onClick={()=>{toggleInFavorite(id)}}/>}</p> 
             <h3>
                {title}
            </h3>
            <StyledImg src = {url} alt ={title}/>
            <p>{price}</p>
            <p>{description}</p>
            <p>{article}</p>
            
        <Button text="add to card" backgroundColor="blue" onClick={()=>{onClick(id)}}/>
        </li>  
        )
        
}
}
Product.propTypes = {
  title:PropTypes.string,
  url:PropTypes.string,
  article:PropTypes.string,
  price:PropTypes.string,
  description:PropTypes.string,
  onClick:PropTypes.func,
  id:PropTypes.string,
  toggleInFavorite:PropTypes.func,
  isInFav:PropTypes.bool
}
export default Product