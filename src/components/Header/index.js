import { PureComponent } from "react";
import PropTypes from "prop-types";
import { StyledHeader } from "./styled";
import {AiOutlineShoppingCart,AiOutlineHeart} from "react-icons/ai"

class Header extends PureComponent {

  render() {
    const { favCount,cartCount} = this.props
    const count = cartCount.reduce((acc,curr)=>{
        acc += curr.counter
        return acc
    },0)
    return <StyledHeader ><p><AiOutlineHeart/> - {favCount}</p><p><AiOutlineShoppingCart/> - {count}</p></StyledHeader>
  } 
}
Header.propTypes = {
  favCount:PropTypes.number,
  cartCount:PropTypes.number,
}
export default Header;