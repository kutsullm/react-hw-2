import { PureComponent } from "react"
import Product from "../product/product"
import PropTypes from "prop-types";
class ProductList extends PureComponent {
  
render() {
    const {products,showModal,toggleInFavorite} = this.props
    return(
       <> <ul>
         {products.map((product)=>{
          
        return <Product onClick={showModal} toggleInFavorite={toggleInFavorite} key = {product.id} {...product}/>
        
        })} 
    </ul>  </>
   )

}

    

}
ProductList.propTypes = {
  products:PropTypes.array,
  showModal:PropTypes.func,
  toggleInFavorite:PropTypes.func

}
export default ProductList