import styled from "styled-components"

export const StyledModal = styled.div`
font-family: 'Open-sans', sans-serif;
    background-color: #E74C3C;
    border-radius: 5px;
    max-width: 515px;
`
export const ModalBckg = styled.div `
display:flex;
justify-content:center;
align-items:center;
position: fixed; /* Stay in place */
z-index: 1; /* Sit on top */
left: 0;
top: 0;
width: 100%; /* Full width */
height: 100%; /* Full height */
overflow: auto; /* Enable scroll if needed */
background-color: rgb(0,0,0); /* Fallback color */
background-color: rgba(0,0,0,0.4);
`
export const ModalContent = styled.div`
min-height: 185px;
display: flex;
padding: 0 30px;
flex-direction: column;
text-align: center;
justify-content: space-around;
`
export const StyledParagraph = styled.p `
font-size: 15px;
line-height: 30px;
color: #FFF;
`
export const StyledHeader = styled.div`

min-height: 70px;
padding: 0 30px;
background-color: rgba(0, 0, 0, .15);
display: flex;
justify-content: space-between;
align-items: center;
border-top-left-radius: 5px;
border-top-right-radius: 5px;
`
export const StyledH = styled.h2`
font-weight: bold;
font-size: 22px;
line-height: 60px;
color: #FFF;
-webkit-text-stroke: 1px black;
 `
 export const StyledButton = styled.button`
 font-size: 30px;
 border: 0;
 background-color: transparent;
 `