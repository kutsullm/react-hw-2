import Button from "../Button";
import { PureComponent } from "react";
import { StyledModal,ModalBckg,ModalContent,StyledParagraph,StyledHeader,StyledH,StyledButton } from "./styled";
import PropTypes from "prop-types";
class Modal extends PureComponent {
  render () {
    const { title, text, hideFn,hasCloseButton,current,addToCart } = this.props;
    const optionalCeneterd = {
      'justifyContent': hasCloseButton ? 'space-between' : 'center',
    }
    return (
      <ModalBckg onClick={() => hideFn()}>
        <StyledModal onClick={e => e.stopPropagation()}>
          <StyledHeader style={optionalCeneterd}>
            <StyledH>{title}</StyledH>
            {hasCloseButton && <StyledButton onClick={() => hideFn()}>&#10761;</StyledButton>}
          </StyledHeader>
          <ModalContent>
           <StyledParagraph>{text}</StyledParagraph>
            <div>
            <Button text='Ok' backgroundColor='#008CBA' onClick={() => {
              addToCart(current)
            }} />,
            <Button text='Cancel' backgroundColor='#e7e7e7' onClick={() => {
              hideFn();
            }} />
            </div>
          </ModalContent>
        </StyledModal>
      </ModalBckg>
    )
  }
}
Modal.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  id: PropTypes.number,
  hideFn: PropTypes.func,
  addToCart: PropTypes.func,
  hasCloseButton: PropTypes.bool,
}
export default Modal;